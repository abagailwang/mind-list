# Generated by Django 4.1.4 on 2022-12-07 20:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("projects", "0002_task"),
    ]

    operations = [
        migrations.DeleteModel(
            name="Task",
        ),
    ]
